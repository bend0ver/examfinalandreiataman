import java.sql.*;

/* nu am putut da run al codul SQL, nu aveam mysql instalat pe pc
   daca ceva se pierde codul sql, l-am lasat intr-un comentariu la sfarsitul acestui file
 */

public class Main {
    private static Connection connect = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static String url = "jdbc:mysql://localhost:3306/DJ2112RO";
    private static String user = "root", pass = "root";

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = getConnection(url,user,pass);
            statement = connect.createStatement();
            resultSet = statement.executeQuery("select * from STUDENTI");
            while(resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String family = resultSet.getString(3);
                Time time = resultSet.getTime(4);

                System.out.println(id + " " + name + " " + family + " " + time);
            }
        } catch ( SQLException e) {
            e.printStackTrace();
        } catch ( ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
/*
CREATE DATABASE DJ2112RO;
USE DJ2112RO;

CREATE TABLE STUDENTI
(
    ID INT PRIMARY KEY AUTO_INCREMENT,
    PRENUME VARCHAR(50) NOT NULL,
    FAMILIE VARCHAR(50) NOT NULL,
    BIRTH TIMESTAMP NOT NULL
)

INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Andrei', 'Ataman', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Dinu', 'Bolduratu', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Cristian', 'Condrea', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Cristi', 'Vizitiu', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Cristian', 'Istrati', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Nicolae', 'Lupu', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Daniil', 'Pogosov', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Rostislav', 'Sanduta', '2006-10-09 12:00:00')
INSERT INTO STUDENTI(PRENUME, FAMILIE, BIRTH) VALUES('Dragos', 'Tulbure', '2006-10-09 12:00:00')

 */
